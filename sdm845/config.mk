PRODUCT_SOONG_NAMESPACES += \
    vendor/oneplus/apps/sdm845

PRODUCT_PACKAGES += \
    OnePlusCamera \
    OnePlusCameraService \
    OnePlusGallery

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system/etc/permissions/privapp-permissions-com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-com.oneplus.camera.service.xml \
    $(LOCAL_PATH)/system_ext/etc/permissions/privapp-permissions-com.oneplus.camera_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-com.oneplus.camera_ext.xml \
    $(LOCAL_PATH)/system_ext/etc/permissions/privapp-permissions-com.oneplus.gallery_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-com.oneplus.gallery_ext.xml \
    $(LOCAL_PATH)/system_ext/etc/sysconfig/hiddenapi-package-whitelist-com.oneplus.camera_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist-com.oneplus.camera_ext.xml
